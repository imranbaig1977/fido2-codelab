package com.example.android.fido2.api

import android.util.EventLog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.nio.charset.Charset


/*
* CurlInterceptor forms a curl command
* This interceptor class will only be added to HTTP Client in debug mode.
*
* */

class CurlInterceptor : Interceptor
{
    var gson = GsonBuilder().setPrettyPrinting().create()

    override fun intercept(chain: Interceptor.Chain): Response {

        Timber.d(" **** ->>Request to server -> ****")

        val request = chain.request()
        var response = chain.proceed(request)

        var curl = "curl -v -X  ${request.method()}"

        val headers = request.headers()

        for ( i in 0..(headers.size() -1) ){
            curl = "${curl} -H \"${headers.name(i)}: ${headers.value(i)}\""
        }

        val requestBody = request.body()
        if (requestBody != null) {
            val buffer = Buffer()
            requestBody.writeTo(buffer)
            var charset: Charset =
                Charset.forName("UTF-8")
            curl = "${curl} --data '${buffer.readString(charset).replace("\n", "\\n")}'"
        }


        Timber.d("$curl ${request.url()}")
        Timber.d("response status code ${response.code()} message: ${response.message()}")

        EventBus.getDefault().post( LogEvent( "\n**** ->>Request to server -> **** \n" + "$curl ${request.url()}" ))


        dumbHeaders(response)

        var responseBody = response?.body()

        if(responseBody != null )
        {
            var responseBodyString = responseBody?.string()

                response = response.newBuilder()
                    .body(
                        ResponseBody.create(
                            responseBody?.contentType(),
                            responseBodyString.toByteArray()
                        )
                    )
                    .build()


            responseBodyString = gson.toJson(responseBodyString)

            responseBodyString =  responseBodyString.replace("\\", "" )


            Timber.d("response json -> \n $responseBodyString")
            EventBus.getDefault().post(LogEvent( "\n **** << Response from server **** \n" + responseBodyString ) )


        }

        Timber.d(" **** << Response from server ****")

        return response
    }



    fun dumbHeaders(response: Response) {
        try {
            if (response.headers() != null) {

                for (headerName in response.headers().names()) {
                    for (headerValue in response.headers(headerName)) {
                        Timber.d("Header $headerName : $headerValue")
                    }
                }
            }
        }catch (ex: Exception){

        }
    }

}